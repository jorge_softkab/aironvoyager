<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use TCG\Voyager\Events\BreadDataUpdated;
use TCG\Voyager\Events\BreadDataAdded;
use TCG\Voyager\Facades\Voyager;
use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Support\Str;
use TCG\Voyager\Http\Controllers\VoyagerBaseController as BasVoyagerBaseController;
class ImagesController extends BasVoyagerBaseController
{




    public function update(Request $request, $id)
    {
        $slug = $this->getSlug($request);
        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();
        // Compatibility with Model binding.
        $id = $id instanceof Model ? $id->{$id->getKeyName()} : $id;
        $data = call_user_func([$dataType->model_name, 'findOrFail'], $id);
        // Check permission
        $this->authorize('edit', $data);
        // Validate fields with ajax
        $val = $this->validateBread($request->all(), $dataType->editRows, $dataType->name, $id);

        foreach ($dataType->addRows as $row) {

            if ($row->type == 'image' ) {
                $content = $row->field;
            }
            }
        if ($val->fails()) {
            return response()->json(['errors' => $val->messages()]);
        }
        if (!$request->ajax()) {
           $this->insertUpdateData($request, $slug, $dataType->editRows, $data);
           event(new BreadDataUpdated($dataType, $data));
           if ($request->hasFile($content)) {
            //$fecha=strval(date('d-m-Y'));
                $nombre=Str::slug($request->nombre);
                $Imagenes =\App\Producto::find($id);
                $Imagenes->url_image=\URL::to('/images').'/'.Str::slug($nombre).'.jpg';
                $Imagenes->save();
                Image::make($request->file($content))->save('images/'.$nombre.'.jpg');
            }

          return redirect()
          ->route("voyager.{$dataType->slug}.index")
          ->with([
              'message'    => __('voyager::generic.successfully_updated')." {$dataType->display_name_singular}",
              'alert-type' => 'success',
          ]);

         // return  $dataType->editRows;


        }




    }

    public function store(Request $request)
    {
        $slug = $this->getSlug($request);
        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();
        // Check permission
        $this->authorize('add', app($dataType->model_name));
        // Validate fields with ajax
        $val = $this->validateBread($request->all(), $dataType->addRows);
        foreach ($dataType->addRows as $row) {

                        if ($row->type == 'image' ) {
                            $content = $row->field;
                        }
         }
        if ($val->fails()) {
            return response()->json(['errors' => $val->messages()]);
        }
        if (!$request->has('_validate')) {
            $data = $this->insertUpdateData($request, $slug, $dataType->addRows, new $dataType->model_name());
            event(new BreadDataAdded($dataType, $data));

            if ($request->hasFile($content)) {
                //$fecha=strval(date('d-m-Y'));
                    $nombre=Str::slug($request->nombre);
                    $Imagenes =\App\Producto::find($data->id);

                    Image::make(\URL::to('/storage').'/'.$Imagenes->$content)->save('images/'.$nombre.'.jpg');
                    $Imagenes->url_image=\URL::to('/images').'/'.Str::slug($nombre).'.jpg';
                    $Imagenes->save();
            }

            if ($request->ajax()) {
                return response()->json(['success' => true, 'data' => $data]);
            }
            return redirect()
                ->route("voyager.{$dataType->slug}.index")
                ->with([
                        'message'    => __('voyager::generic.successfully_added_new')." {$dataType->display_name_singular}",
                        'alert-type' => 'success',
                    ]);



       // return $content;
        //return $Imagenes;
        }
    }

}